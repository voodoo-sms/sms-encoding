<?php

namespace VoodooSMS\SmsEncoding\Abstracts;

abstract class AbstractEncodedMessage
{
    /**
     * @var string
     */
    public $payload;

    public function __construct(string $payload)
    {
        $this->payload = $this->replaceLineBreaks($payload);
    }

    /**
     * Get the payload
     *
     * @return string
     */
    public function getPayload(): string
    {
        return $this->payload;
    }

    /**
     * Set the payload.
     *
     * @param string $payload
     * @return static
     */
    public function setPayload(string $payload)
    {
        $this->payload = $payload;

        return $this;
    }

    /**
     * Get the number of segments a message contains.
     *
     * @return int
     */
    abstract public function getNumberOfSegments(): int;

    /**
     * Get the total length of a message.
     * N.B. This takes into account the weighting of a character:
     * - { counts as 2
     * - emojis count as 2+
     *
     * @return int
     */
    abstract public function getLength(): int;

    /**
     * Get the total number of characters in the message.
     *
     * @return int
     */
    final public function getNumberOfCharacters(): int
    {
        return mb_strlen($this->payload);
    }

    /**
     * Return the number of segments in a message based on supplied
     * limits.
     *
     * @param int $length   The total length of the message.
     * @param int $singleMessageLimit   The max (non-inclusive) number of characters for a single message.
     * @param int $multiMessageLength   The number of characters per message for a multi-segment message.
     * @return int
     */
    final protected static function getNumberOfSegmentsForLimits(
        int $length,
        int $singleMessageLimit,
        int $multiMessageLength
    ): int {
        if ($length > $singleMessageLimit) {
            return ceil(
                $length / $multiMessageLength
            );
        }

        return 1;
    }

    /**
     * Replace line breaks in the message with PHP_EOL
     *
     * @param string $payload
     * @return string
     */
    private function replaceLineBreaks(string $payload): string
    {
        return str_replace(
            ['\r\n', '\n'],
            PHP_EOL,
            $payload
        );
    }
}
