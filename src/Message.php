<?php

namespace VoodooSMS\SmsEncoding;

use VoodooSMS\SmsEncoding\Abstracts\AbstractEncodedMessage;
use VoodooSMS\SmsEncoding\Lib\Gsm7Characters;
use VoodooSMS\SmsEncoding\Messages\Gsm7Message;
use VoodooSMS\SmsEncoding\Messages\UnicodeMessage;

/**
 * @method bool isGsm7() Determines if all characters in the payload are GSM-7 characters.
 * @method bool isUnicode() Determines if the payload contains non-GSM-7 characters.
 * @method bool isUcs2() Determines if the payload contains non-GSM-7 characters.
 */
class Message extends AbstractEncodedMessage
{
    /**
     * Determines if all characters in the payload are GSM-7 characters.
     *
     * @return bool
     */
    public function is7Bit(): bool
    {
        foreach (mb_str_split($this->payload) as $character) {
            if (
                !Gsm7Characters::isValidCharacter($character)
            ) {
                return false;
            }
        }

        return true;
    }

    /**
     * Determines if the payload contains non-GSM-7 characters.
     *
     * @return bool
     */
    public function is8Bit(): bool
    {
        return !$this->is7Bit();
    }

    /**
     * Get the total length of a message.
     *
     * @return int
     */
    public function getLength(): int
    {
        return $this->is7Bit($this->payload)
            ? (new Gsm7Message($this->payload))->getLength()
            : (new UnicodeMessage($this->payload))->getLength();
    }

    /**
     * Gte the number of segments a message contains.
     *
     * @param string $payload
     * @return int
     */
    public function getNumberOfSegments(): int
    {
        return $this->is7Bit()
            ? (new Gsm7Message($this->payload))->getNumberOfSegments()
            : (new UnicodeMessage($this->payload))->getNumberOfSegments();
    }

    /**
     * @param string $name
     * @param array $args
     * @return void
     */
    public function __call($name, $args)
    {
        if (in_array($name, ['isGsm7'])) {
            return $this->is7Bit();
        }

        if (in_array($name, ['isUnicode', 'isUcs2'])) {
            return $this->is8Bit();
        }

        return $this->$name(...$args);
    }
}
