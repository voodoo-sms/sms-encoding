<?php

namespace VoodooSMS\SmsEncoding\Messages;

use VoodooSMS\SmsEncoding\Abstracts\AbstractEncodedMessage;
use VoodooSMS\SmsEncoding\Lib\Gsm7Characters;

class UnicodeMessage extends AbstractEncodedMessage
{
    public const SINGLE_MESSAGE_LENGTH = 70;
    public const MULTI_MESSAGE_LENGTH = 67;

    /**
     * Get the message length for a unicode message.
     *
     * @return int
     */
    public function getLength(): int
    {
        $length = 0;

        foreach (mb_str_split($this->payload) as $character) {
            $length += Gsm7Characters::isValidCharacter($character)
                ? 1 // When it's being encoded as UCS-2 this is always 1 for a GSM-7 character
                : mb_strlen($character, 'UTF-16');
        }

        return $length;
    }

    /**
     * Calculate the number of segments in a unicode message.
     *
     * @return int
     */
    public function getNumberOfSegments(): int
    {
        return self::getNumberOfSegmentsForLimits(
            $this->getLength(),
            self::SINGLE_MESSAGE_LENGTH,
            self::MULTI_MESSAGE_LENGTH
        );
    }
}
