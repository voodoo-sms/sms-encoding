<?php

namespace VoodooSMS\SmsEncoding\Messages;

use VoodooSMS\SmsEncoding\Abstracts\AbstractEncodedMessage;
use VoodooSMS\SmsEncoding\Lib\Gsm7Characters;

class Gsm7Message extends AbstractEncodedMessage
{
    public const SINGLE_MESSAGE_LENGTH = 160;
    public const MULTI_MESSAGE_LENGTH = 153;

    /**
     * Get the message length for a GSM-7 message.
     *
     * @return int
     */
    public function getLength(): int
    {
        $length = 0;

        foreach (mb_str_split($this->payload) as $character) {
            $length += Gsm7Characters::getCharacterWeighting($character);
        }

        return $length;
    }

    /**
     * Get the number of segments a GSM-7 message contains.
     *
     * @return int
     */
    public function getNumberOfSegments(): int
    {
        return self::getNumberOfSegmentsForLimits(
            $this->getLength(),
            self::SINGLE_MESSAGE_LENGTH,
            self::MULTI_MESSAGE_LENGTH
        );
    }
}
