<?php

namespace VoodooSMS\SmsEncoding\Lib;

use VoodooSMS\SmsEncoding\Exceptions\InvalidGsm7CharacterException;

class Gsm7Characters
{
    /**
     * Hex encoded GSM-7 characters with number of segments they use.
     *
     * There's a useful twilio site that shows this breakdown:
     * @link https://twiliodeved.github.io/message-segment-calculator/
     */
    public static $hexadecimal = [
        '40'        => 1, // @
        'c2a3'      => 1, // £
        '24'        => 1, // $
        'c2a5'      => 1, // ¥
        'c3a8'      => 1, // è
        'c3a9'      => 1, // é
        'c3b9'      => 1, // ù
        'c3ac'      => 1, // ì
        'c3b2'      => 1, // ò
        'c387'      => 1, // Ç
        '0a'        => 1, // Line feed
        'c398'      => 1, // Ø
        'c3b8'      => 1, // ø
        '0d'        => 1, // Carriage return
        'c385'      => 1, // Å
        'c3a5'      => 1, // å
        'ce94'      => 1, // Δ
        '5f'        => 1, // _
        'cea6'      => 1, // Φ
        'ce93'      => 1, // Γ
        'ce9b'      => 1, // Λ
        'cea9'      => 1, // Ω
        'cea0'      => 1, // Π
        'cea8'      => 1, // Ψ
        'cea3'      => 1, // Σ
        'ce98'      => 1, // Θ
        'ce9e'      => 1, // Ξ
        '1b'        => 1, // Escape to extension table
        'c386'      => 1, // Æ
        'c3a6'      => 1, // æ
        'c39f'      => 1, // ß
        'c389'      => 1, // É
        '20'        => 1, // Space
        '21'        => 1, // !
        '22'        => 1, // "
        '23'        => 1, // #
        'c2a4'      => 1, // ¤
        '25'        => 1, // %
        '26'        => 1, // &
        '27'        => 1, // '
        '28'        => 1, // (
        '29'        => 1, // )
        '2a'        => 1, // *
        '2b'        => 1, // +
        '2c'        => 1, // ,
        '2d'        => 1, // -
        '2e'        => 1, // .
        '2f'        => 1, // /
        '30'        => 1, // 0
        '31'        => 1, // 1
        '32'        => 1, // 2
        '33'        => 1, // 3
        '34'        => 1, // 4
        '35'        => 1, // 5
        '36'        => 1, // 6
        '37'        => 1, // 7
        '38'        => 1, // 8
        '39'        => 1, // 9
        '3a'        => 1, // :
        '3b'        => 1, // ;
        '3c'        => 1, // <
        '3d'        => 1, // =
        '3e'        => 1, // >
        '3f'        => 1, // ?
        'c2a1'      => 1, // ¡
        '41'        => 1, // A
        '42'        => 1, // B
        '43'        => 1, // C
        '44'        => 1, // D
        '45'        => 1, // E
        '46'        => 1, // F
        '47'        => 1, // G
        '48'        => 1, // H
        '49'        => 1, // I
        '4a'        => 1, // J
        '4b'        => 1, // K
        '4c'        => 1, // L
        '4d'        => 1, // M
        '4e'        => 1, // N
        '4f'        => 1, // O
        '50'        => 1, // P
        '51'        => 1, // Q
        '52'        => 1, // R
        '53'        => 1, // S
        '54'        => 1, // T
        '55'        => 1, // U
        '56'        => 1, // V
        '57'        => 1, // W
        '58'        => 1, // X
        '59'        => 1, // Y
        '5a'        => 1, // Z
        'c384'      => 1, // Ä
        'c396'      => 1, // Ö
        'c391'      => 1, // Ñ
        'c39c'      => 1, // Ü
        'c2a7'      => 1, // §
        'c2bf'      => 1, // ¿
        '61'        => 1, // a
        '62'        => 1, // b
        '63'        => 1, // c
        '64'        => 1, // d
        '65'        => 1, // e
        '66'        => 1, // f
        '67'        => 1, // g
        '68'        => 1, // h
        '69'        => 1, // i
        '6a'        => 1, // j
        '6b'        => 1, // k
        '6c'        => 1, // l
        '6d'        => 1, // m
        '6e'        => 1, // n
        '6f'        => 1, // o
        '70'        => 1, // p
        '71'        => 1, // q
        '72'        => 1, // r
        '73'        => 1, // s
        '74'        => 1, // t
        '75'        => 1, // u
        '76'        => 1, // v
        '77'        => 1, // w
        '78'        => 1, // x
        '79'        => 1, // y
        '7a'        => 1, // z
        'c3a4'      => 1, // ä
        'c3b6'      => 1, // ö
        'c3b1'      => 1, // ñ
        'c3bc'      => 1, // ü
        'c3a0'      => 1, // à
        '0c'        => 2, // Form feed
        '5e'        => 2, // ^
        '7b'        => 2, // {
        '7d'        => 2, // }
        '5c'        => 2, // \
        '5b'        => 2, // [
        '7e'        => 2, // ~
        '5d'        => 2, // ]
        '7c'        => 2, // |
        'e282ac'    => 2, // €
    ];

    /**
     * Plaintext GSM-7 characters with hex-encoded version.
     *
     * There are a few missing, such as Line feed, carriage return etc.
     * So try to use the hex versions when using this array manually.
     */
    public static $plaintext = [
        '@'     => '40',
        '£'     => 'c2a3',
        '$'     => '24',
        '¥'     => 'c2a5',
        'è'     => 'c3a8',
        'é'     => 'c3a9',
        'ù'     => 'c3b9',
        'ì'     => 'c3ac',
        'ò'     => 'c3b2',
        'Ç'     => 'c387',
        'Ø'     => 'c398',
        'ø'     => 'c3b8',
        'Å'     => 'c385',
        'å'     => 'c3a5',
        'Δ'     => 'ce94',
        '_'     => '5f',
        'Φ'     => 'cea6',
        'Γ'     => 'ce93',
        'Λ'     => 'ce9b',
        'Ω'     => 'cea9',
        'Π'     => 'cea0',
        'Ψ'     => 'cea8',
        'Σ'     => 'cea3',
        'Θ'     => 'ce98',
        'Ξ'     => 'ce9e',
        'Æ'     => 'c386',
        'æ'     => 'c3a6',
        'ß'     => 'c39f',
        'É'     => 'c389',
        ' '     => '20',
        '!'     => '21',
        '"'     => '22',
        '#'     => '23',
        '¤'     => 'c2a4',
        '%'     => '25',
        '&'     => '26',
        '\''    => '27',
        '('     => '28',
        ')'     => '29',
        '*'     => '2a',
        '+'     => '2b',
        ','     => '2c',
        '-'     => '2d',
        '.'     => '2e',
        '/'     => '2f',
        '0'     => '30',
        '1'     => '31',
        '2'     => '32',
        '3'     => '33',
        '4'     => '34',
        '5'     => '35',
        '6'     => '36',
        '7'     => '37',
        '8'     => '38',
        '9'     => '39',
        ':'     => '3a',
        ';'     => '3b',
        '<'     => '3c',
        '='     => '3d',
        '>'     => '3e',
        '?'     => '3f',
        '¡'     => 'c2a1',
        'A'     => '41',
        'B'     => '42',
        'C'     => '43',
        'D'     => '44',
        'E'     => '45',
        'F'     => '46',
        'G'     => '47',
        'H'     => '48',
        'I'     => '49',
        'J'     => '4a',
        'K'     => '4b',
        'L'     => '4c',
        'M'     => '4d',
        'N'     => '4e',
        'O'     => '4f',
        'P'     => '50',
        'Q'     => '51',
        'R'     => '52',
        'S'     => '53',
        'T'     => '54',
        'U'     => '55',
        'V'     => '56',
        'W'     => '57',
        'X'     => '58',
        'Y'     => '59',
        'Z'     => '5a',
        'Ä'     => 'c384',
        'Ö'     => 'c396',
        'Ñ'     => 'c391',
        'Ü'     => 'c39c',
        '§'     => 'c2a7',
        '¿'     => 'c2bf',
        'a'     => '61',
        'b'     => '62',
        'c'     => '63',
        'd'     => '64',
        'e'     => '65',
        'f'     => '66',
        'g'     => '67',
        'h'     => '68',
        'i'     => '69',
        'j'     => '6a',
        'k'     => '6b',
        'l'     => '6c',
        'm'     => '6d',
        'n'     => '6e',
        'o'     => '6f',
        'p'     => '70',
        'q'     => '71',
        'r'     => '72',
        's'     => '73',
        't'     => '74',
        'u'     => '75',
        'v'     => '76',
        'w'     => '77',
        'x'     => '78',
        'y'     => '79',
        'z'     => '7a',
        'ä'     => 'c3a4',
        'ö'     => 'c3b6',
        'ñ'     => 'c3b1',
        'ü'     => 'c3bc',
        'à'     => 'c3a0',
        '^'     => '5e',
        '{'     => '7b',
        '}'     => '7d',
        '\\'    => '5c',
        '['     => '5b',
        '~'     => '7e',
        ']'     => '5d',
        '|'     => '7c',
        '€'     => 'e282ac',
    ];

    /**
     * Determine if a character is a valid GSM-7 character.
     *
     * @param string $character
     * @param bool $hexadecimal Is the character you are passing already hex-encoded?
     * @return bool
     */
    public static function isValidCharacter(string $character, bool $hexadecimal = false): bool
    {
        if (!$hexadecimal) {
            $character = bin2hex($character);
        }

        return in_array(
            $character,
            array_keys(self::$hexadecimal)
        );
    }

    /**
     * Get the weighting for a character.
     *
     * @param string $character
     * @param bool $hexadecimal Is the character you are passing already hex-encoded?
     * @return int
     * @throws Voodoosms\SmsEncoding\Exceptions\InvalidGsm7CharacterException
     */
    public static function getCharacterWeighting(string $character, bool $hexadecimal = false): int
    {
        if (!$hexadecimal) {
            $character = bin2hex($character);
        }

        if (!self::isValidCharacter($character, true)) {
            throw new InvalidGsm7CharacterException(
                'The character \'' . hex2bin($character) . '\' is not a valid GSM-7 character'
            );
        }

        return self::$hexadecimal[$character];
    }
}
