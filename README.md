# Sms Encoding Library

Get the length, number of characters, number of segments and encoding of an SMS.
## Installation

```shell
composer require voodoosms/sms-encoding
```

## Usage

### `Message`

Available methods:

```php
$message = new Message(string $payload)

$message->is7Bit();
$message->isGsm7(); // alias of is7Bit()

$message->is8Bit();
$message->isUcs2(); // alias of is8Bit()
$message->isUnicode(); // alias of is8Bit()

$message->getLength();
$message->getNumberOfSegments();
$message->getNumberOfCharacters();
```

### `Gsm7Message`

Available methods:
```php
$message = new Gsm7Message(string $payload)

$message->getLength();
$message->getNumberOfSegments();
$message->getNumberOfCharacters();
```

### `UnicodeMessage`

Available methods:

```php
$message = new UnicodeMessage(string $payload)

$message->getLength();
$message->getNumberOfSegments();
$message->getNumberOfCharacters();
```

### `Gsm7Characters`

Available methods:
```php
Gsm7Characters::isValidCharacter(string $character, bool $hexadecimal = false);
Gsm7Characters::getCharacterWeighting(string $character, bool $hexadecimal = false);
Gsm7Characters::$hexadecimal; // An array keyed by the hex-encoded version, with the value being the characters weighting
Gsm7Characters::$plaintext; // An array keyed by the plaintext version, with the value being the hex-encoded version
```

```php
$testValid = 'T';
$testValidHex = '54';
$testInvalid = '🚀';
$testInvalidHex = 'f09f9a80';

Gsm7Characters::isValidCharacter($testValid); // true
Gsm7Characters::isValidCharacter($testValidHex, true); // true
Gsm7Characters::isValidCharacter($testInvalidHex); // false
Gsm7Characters::isValidCharacter($testInvalidHex, true); // false

Gsm7Characters::getCharacterWeighting($testValid); // 1
Gsm7Characters::getCharacterWeighting($testValidHex, true); // 1
Gsm7Characters::getCharacterWeighting($testInvalidHex); // Throws InvalidGsm7CharacterException
Gsm7Characters::getCharacterWeighting($testInvalidHex, true); // Throws InvalidGsm7CharacterException
```

You likely will never need to use `Gsm7Message` or `UnicodeMessage` as the `Message` class is just a proxy for these two classes.
