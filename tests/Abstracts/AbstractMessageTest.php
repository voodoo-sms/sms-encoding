<?php

namespace VoodooSMS\SmsEncoding\Tests\Abstracts;

use VoodooSMS\SmsEncoding\Tests\TestCase;

abstract class AbstractMessageTest extends TestCase
{
    /**
     * An array of test data, in the format:
     *  [
     *      [
     *          'body' => ...,
     *          'length' => ...,
     *          'characters' => ...,
     *          'segments' => ...,
     *      ]
     *  ]
     */
    protected array $data;

    public function setUp(): void
    {
        parent::setUp();

        $this->data = require($this->getDataPath());
    }

    /**
     * Returns the path to the test data.
     *
     * @return string
     */
    abstract public function getDataPath(): string;
}
