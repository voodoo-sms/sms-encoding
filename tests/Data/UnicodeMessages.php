<?php

return [
    [
        'body' => 'Hello HelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHel🚀',
        'length' => 81,
        'characters' => 80,
        'segments' => 2,
    ],
    [
        'body' => 'Hello HelloHelloHelloHelloHelloHelloHelloHelloHelloHel🚀',
        'length' => 56,
        'characters' => 55,
        'segments' => 1,
    ],
    [
        'body' => 'Hello HelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelHelloHello HelloHelloHelloHelloHelloHeloHelloHelloHellloHell🚀',
        'length' => 141,
        'characters' => 140,
        'segments' => 3,
    ],
];
