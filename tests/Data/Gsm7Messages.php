<?php

return [
    [
        'body' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lacinia orci tortor, porta accumsan nulla sodales quis.',
        'length' => 120,
        'characters' => 120,
        'segments' => 1,
    ],
    [
        'body' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lacinia orci tortor, porta accumsan nulla sodales quis. porta accumsan nulla sodales quis. port',
        'length' => 160,
        'characters' => 160,
        'segments' => 1,
    ],
    [
        'body' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lacinia orci tortor, porta accumsan nulla sodales quis. porta accumsan nulla sodales quis. porta',
        'length' => 161,
        'characters' => 161,
        'segments' => 2,
    ],
    [
        'body' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lacinia orci tortor, porta accumsan nulla sodales quis. porta accumsan nulla sodales quis. poh}',
        'length' => 161,
        'characters' => 160,
        'segments' => 2,
    ],
    [
        'body' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.\nInteger lacinia orci tortor, porta accumsan nulla sodales',
        'length' => 114,
        'characters' => 114,
        'segments' => 1,
    ],
    [
        'body' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.\nInteger lacinia orci tortor, porta\naccumsan nulla sodales',
        'length' => 114,
        'characters' => 114,
        'segments' => 1,
    ],
    [
        'body' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.\r\nInteger lacinia orci tortor, porta accumsan nulla sodales',
        'length' => 114,
        'characters' => 114,
        'segments' => 1,
    ],
    [
        'body' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.\r\nInteger lacinia orci tortor, porta\r\naccumsan nulla sodales',
        'length' => 114,
        'characters' => 114,
        'segments' => 1,
    ],
];
