<?php

namespace VoodooSMS\SmsEncoding\Tests\Unit;

use VoodooSMS\SmsEncoding\Message;
use VoodooSMS\SmsEncoding\Tests\TestCase;

class MessageTest extends TestCase
{
    private array $gsmMessages;
    private array $unicodeMessages;

    public function setUp(): void
    {
        parent::setUp();

        $this->gsmMessages = require(__DIR__ . '/../Data/Gsm7Messages.php');
        $this->unicodeMessages = require(__DIR__ . '/../Data/UnicodeMessages.php');
    }

    public function test_it_identifies_gsm7_messages_correctly()
    {
        foreach ($this->gsmMessages as $message) {
            $this->assertTrue(
                (new Message($message['body']))->is7Bit()
            );
            $this->assertTrue(
                (new Message($message['body']))->isGsm7()
            );
        }
    }

    public function test_it_identifies_unicode_messages_correctly()
    {
        foreach ($this->unicodeMessages as $message) {
            $this->assertTrue(
                (new Message($message['body']))->is8Bit()
            );
            $this->assertTrue(
                (new Message($message['body']))->isUcs2()
            );
            $this->assertTrue(
                (new Message($message['body']))->isUnicode()
            );
        }
    }

    public function test_it_doesnt_classify_valid_gsm7_messages_as_unicode()
    {
        foreach ($this->gsmMessages as $message) {
            $this->assertFalse(
                (new Message($message['body']))->is8Bit()
            );
        }
    }

    public function test_it_doesnt_classify_valid_unicode_messages_as_gsm7()
    {
        foreach ($this->unicodeMessages as $message) {
            $this->assertFalse(
                (new Message($message['body']))->is7Bit()
            );
        }
    }
}
