<?php

namespace VoodooSMS\SmsEncoding\Tests\Unit\Messages;

use VoodooSMS\SmsEncoding\Messages\UnicodeMessage;
use VoodooSMS\SmsEncoding\Tests\Abstracts\AbstractMessageTest;

class UnicodeMessageTest extends AbstractMessageTest
{
    public function test_it_correctly_determines_the_message_length()
    {
        foreach ($this->data as $sms) {
            $this->assertEquals(
                $sms['length'],
                (new UnicodeMessage($sms['body']))->getLength()
            );
        }
    }

    public function test_it_correctly_determines_the_message_segments()
    {
        foreach ($this->data as $sms) {
            $this->assertEquals(
                $sms['segments'],
                (new UnicodeMessage($sms['body']))->getNumberOfSegments()
            );
        }
    }

    public function test_it_correctly_determines_the_message_characters()
    {
        foreach ($this->data as $sms) {
            $this->assertEquals(
                $sms['characters'],
                (new UnicodeMessage($sms['body']))->getNumberOfCharacters()
            );
        }
    }

    public function getDataPath(): string
    {
        return __DIR__ . '/../../Data/UnicodeMessages.php';
    }
}
