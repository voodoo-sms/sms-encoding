<?php

namespace VoodooSMS\SmsEncoding\Tests\Unit\Messages;

use VoodooSMS\SmsEncoding\Exceptions\InvalidGsm7CharacterException;
use VoodooSMS\SmsEncoding\Messages\Gsm7Message;
use VoodooSMS\SmsEncoding\Tests\Abstracts\AbstractMessageTest;

class Gsm7MessageTest extends AbstractMessageTest
{
    public function test_it_correctly_determines_the_message_length()
    {
        foreach ($this->data as $sms) {
            $this->assertEquals(
                $sms['length'],
                (new Gsm7Message($sms['body']))->getLength()
            );
        }
    }

    public function test_it_correctly_determines_the_message_segments()
    {
        foreach ($this->data as $sms) {
            $this->assertEquals(
                $sms['segments'],
                (new Gsm7Message($sms['body']))->getNumberOfSegments()
            );
        }
    }

    public function test_it_correctly_determines_the_message_characters()
    {
        foreach ($this->data as $sms) {
            $this->assertEquals(
                $sms['characters'],
                (new Gsm7Message($sms['body']))->getNumberOfCharacters()
            );
        }
    }

    public function test_it_throws_an_exception_when_passing_a_unicode_message()
    {
        $this->expectException(InvalidGsm7CharacterException::class);

        (new Gsm7Message('Test 🚀'))->getLength();
    }

    public function getDataPath(): string
    {
        return __DIR__ . '/../../Data/Gsm7Messages.php';
    }
}
