<?php

namespace VoodooSMS\SmsEncoding\Tests\Unit;

use VoodooSMS\SmsEncoding\Exceptions\InvalidGsm7CharacterException;
use VoodooSMS\SmsEncoding\Lib\Gsm7Characters;
use VoodooSMS\SmsEncoding\Tests\TestCase;

class Gsm7CharactersTest extends TestCase
{
    public function test_it_correctly_identitfies_characters_when_passing_them_as_plaintext()
    {
        foreach (Gsm7Characters::$plaintext as $char => $hex) {
            $this->assertTrue(
                Gsm7Characters::isValidCharacter($char)
            );
        }
    }

    public function test_it_correctly_identitfies_characters_when_passing_them_as_hexadecimal()
    {
        foreach (Gsm7Characters::$plaintext as $char => $hex) {
            $this->assertTrue(
                Gsm7Characters::isValidCharacter(bin2hex($char), true)
            );
        }
    }

    public function test_it_throws_an_exception_when_getting_the_weighting_for_an_invalid_character()
    {
        $this->expectException(InvalidGsm7CharacterException::class);

        Gsm7Characters::getCharacterWeighting('🚀');
    }

    public function test_it_returns_false_when_passing_an_invalid_character()
    {
        $this->assertFalse(
            Gsm7Characters::isValidCharacter('🚀')
        );
        $this->assertFalse(
            Gsm7Characters::isValidCharacter(bin2hex('🚀'), true)
        );
    }
}
